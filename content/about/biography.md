+++
title = "Biography"
date = 2024-01-19
+++

# Bio: Duke Graduate Student in Computer Science and AI Enthusiast

## Introduction

I am a dedicated graduate student at Duke University, fueled by a profound passion for computer science and artificial intelligence (AI). My academic journey is a dynamic exploration of cutting-edge technologies, emphasizing a commitment to academic excellence, research, and innovation.

## Academic Excellence

With a solid foundation in algorithms, programming languages, and software engineering, my academic pursuits at Duke have equipped me with a comprehensive understanding of computer science. This foundational knowledge serves as a springboard for my exploration into the dynamic and evolving field of artificial intelligence.

## Research Initiatives

My enthusiasm for AI extends beyond the classroom as I actively engage in research initiatives. Delving into machine learning, neural networks, and natural language processing, I seek to unravel the complexities of AI, exploring its potential applications and ethical implications in diverse domains.

## Community Engagement

I contribute actively to the vibrant tech community at Duke, participating in coding competitions, hackathons, and collaborative projects. This hands-on engagement not only hones my technical skills but also fosters a spirit of innovation and teamwork.

## Interdisciplinary Approach

Recognizing the interdisciplinary nature of modern challenges, I leverage my technical expertise to contribute meaningfully to projects at the intersection of data science, cybersecurity, and human-computer interaction.

## STEM Advocacy

Beyond my technical pursuits, I am deeply committed to STEM education advocacy. Actively involved in outreach programs, I mentor and inspire aspiring minds, sharing my knowledge and experiences to ignite a passion for computer science and AI in the next generation of innovators.

