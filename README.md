# IDS721 Individual Project

This project is a personal homepage showcase based on Zola. Please make sure that zola is already installed if you want to run it locally:
```shell
# on mac
brew install zola
zola --version
```

## Starting the Website

To launch the website locally, navigate to the "ids721-individual-project1-xc202" directory using the command `cd ids721-individual-project1-xc202`, and then run `zola serve`. The website will be accessible at http://127.0.0.1:1111/.

Optionally, you can access the website via the following link: https://ids721-individual-project1-dukeaiml-ids721-9c695ce8687307dd2e56.gitlab.io. The web app is deployed on Gitlab and Gitlab workflow to build and deploy is enabled once new changes are pushed. The implemented workflow can be viewed in `.gitlab-ci.yml`, which is based on the latest ubuntu image.

The website is divided into three main sections: the `personal homepage`, the `About Me` page, and the `My Projects` page. You can navigate to these sections by clicking on the corresponding buttons.

**Note:** While SCSS files are used in the development of the website, they are compiled into CSS files for rendering the site's interface. The CSS file used for styling is "style.css" located in the "ids721-individual-project1-xc202/usage" directory.

## Host on Netlify

The web app is successfully hosted on Netlify, you can view the web pages by visiting https://myblog-xiuyuan.netlify.app. `netlify.toml` is a configuration file used by Netlify. I use the netlify.toml file to specify build settings, redirect rules, header file configurations, and more. Also, Netlify allows users to link it with Gitlab repository, allowing auto deployment.

![Screenshot 2024-03-08 at 2.25.28 PM.png](screenshot%2FScreenshot%202024-03-08%20at%202.25.28%20PM.png)

## Demo Video

Check the demonstration of my personal homepage: [Demo Video](https://gitlab.com/dukeaiml/IDS721/ids721-individual-project1-xc202/-/blob/main/Demo%20Video.mov?ref_type=heads). You can also open `Demo Video.mov` under the root dictionary to view the presentation.

## Screenshots

Below are screenshots of the entire website interface:

### Homepage:
![Screenshot 2024-01-27 at 5.04.26 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.04.26%20PM.png)

### Resume:
![Screenshot 2024-01-27 at 5.06.50 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.06.50%20PM.png)

### Projects:
![Screenshot 2024-01-27 at 5.07.32 PM.png](screenshot%2FScreenshot%202024-01-27%20at%205.07.32%20PM.png)